//
//  AppDelegate.h
//  CardManager
//
//  Created by Tiago Giarola Cipriani on 08/09/16.
//  Copyright © 2016 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

