//
//  ITCheckBoxViewController.h
//  CardManager
//
//  Created by Tiago Giarola Cipriani on 12/09/16.
//  Copyright © 2016 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ITCheckBoxViewController : UIViewController

@property BOOL isChecked;

@end
