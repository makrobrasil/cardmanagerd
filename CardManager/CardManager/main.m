//
//  main.m
//  CardManager
//
//  Created by Tiago Giarola Cipriani on 08/09/16.
//  Copyright © 2016 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
