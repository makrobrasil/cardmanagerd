//
//  ITLoginView.m
//  CardManager
//
//  Created by Tiago Giarola Cipriani on 08/09/16.
//  Copyright © 2016 CIT. All rights reserved.
//

#import "ITLoginView.h"

@interface ITLoginView ()

@property (strong, nonatomic) IBOutlet UITextField *LoginTextField;
@property (strong, nonatomic) IBOutlet UITextField *PasswordTextField;

@end

@implementation ITLoginView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
